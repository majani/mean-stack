var express = require('express');
var mongoose = require('mongoose');
var bodyparser = require('body-parser');
var cors = require('cors');

var route = require('./route/routes');

//Instantiate express
var app = express();

//connect to MongoDb

mongoose.connect('mongodb://localhost:27017/shoppinglist', { useNewUrlParser: true, useUnifiedTopology:true, useFindAndModify:false});

//verify connection

mongoose.connection.on('connected', ()=> {
    console.log('MongoDb connected at port 27017');
});

mongoose.connection.on('error', (err) => {
    console.log(err);
});

//specify the port in which the server will be running
const PORT = 3000;

//add middlewares cors
app.use(cors());


//add bodyparser
app.use(bodyparser.json());

//any call with /api will be redirected to the route.js file
app.use('/api', route);




//To get response on Localhost:3000
app.get('/', (req, res)=>{
    res.send('Some more changes');
})

//start the server
app.listen(PORT, () => {
    console.log('Server has been started on port:' + PORT);
})