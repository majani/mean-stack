import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Item } from './item';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(
    private http: HttpClient
  ) { }

  getShoppingItems(){
    return this.http.get<Item[]>('http://localhost:3000/api/items');
  }

  addShoppingItem(newItem){
    // let headers = new HttpHeaders();
    // headers.append('Content-type','application/json');
    return this.http.post('http://localhost:3000/api/item', newItem);

  }

  deleteShoppingItem(id){
    return this.http.delete(`http://localhost:3000/api/item/${id}`);
  }

  updateShoppingitem(newItem){
    return this.http.put(`http://localhost:3000/api/item/${newItem._id}`, newItem);

  }
}
