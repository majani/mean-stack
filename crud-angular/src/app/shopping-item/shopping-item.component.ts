import { Component, OnInit } from '@angular/core';
import { Item } from '../item';
import { DataService } from '../data.service';

@Component({
  selector: 'app-shopping-item',
  templateUrl: './shopping-item.component.html',
  styleUrls: ['./shopping-item.component.css']
})
export class ShoppingItemComponent implements OnInit {
  shoppingItemList: Item[] = [];
  toggleForm: boolean = false;
  selectedItem: Item;

  constructor(
    private dataService: DataService
  ) { }

  getItem() {
    //todo later
  }

  ngOnInit() {
    this.getitems();
  }


  addItem(form) {
    let newItem: Item = {
      itemName: form.value.itemName,
      itemQuantity: form.value.itemQuantity,
      itemBought: false
    }

    this.dataService.addShoppingItem(newItem).subscribe(
      item => {
        console.log(item);
        this.getitems();
      }
    )
  }


  deleteItem(id) {
    this.dataService.deleteShoppingItem(id).subscribe(
      response => {
        console.log(response);
        this.getitems();
      }
    )
  }

  getitems() {
    this.dataService.getShoppingItems().subscribe(
      items => {
        this.shoppingItemList = items;
        console.log('data from dataservice: ' + this.shoppingItemList[0].itemName);
      },
      error => {
        console.log(error);
      }
    )
  }

  editItem(form) {
    let newItem: Item = {
      _id: this.selectedItem._id,
      itemName: form.value.itemName,
      itemQuantity: form.value.itemQuantity,
      itemBought: false
    }
    this.dataService.updateShoppingitem(newItem).subscribe(
      data => {
        console.log(data);
        this.getitems();
        this.toggleForm = !this.toggleForm;

      }


    );


  }

  updateItemCheckBox(item) {
    item.itemBought = !item.itemBought;
  }


  showEditForm(item) {
    this.selectedItem = item;
    this.toggleForm = !this.toggleForm;

    this.dataService.updateShoppingitem(item).subscribe(
      data => {
        console.log(data);
        this.getitems();

      }

    )

  }

}
